package com.nyc.a20180330bm_nycschool;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nyc.a20180330bm_nycschool.application.AppConstant;
import com.nyc.a20180330bm_nycschool.presenter.INYCPresenter;
import com.nyc.a20180330bm_nycschool.presenter.api.IHttpConnection;
import com.nyc.networksdk.JobQueue.PriorityJobQueue;
import com.nyc.networksdk.Listener.ErrorResponseListener;
import com.nyc.networksdk.Listener.ResponseListener;
import com.nyc.networksdk.Network.Constants;
import com.nyc.networksdk.Network.NetworkHandler;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class NetworkHandlerUnitTest {

    @Mock
    NetworkHandler networkHandler;
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void getRequestObject() {
        networkHandler.getRequestObject(Constants.NetworkRequestType.GET, IHttpConnection.IResponseObserver.RequestTypeEnum.GET_SCHOOL_RESULT, AppConstant.SAT_URI, new ResponseListener(new ResponseListener.Listener() {
            @Override
            public void onResponse(String response) {

            }

            @Override
            public void onResponseHeaders(Map<String, String> headers, Object requestTAG) {

            }

            @Override
            public void onResponseObject(NetworkResponse response, Response<String> responseObject, Object requestTAG, Object requestParams) {
                assertEquals(response.statusCode , 200);
                assertNotNull(responseObject);

            }
        }), new ErrorResponseListener(new ErrorResponseListener.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

            @Override
            public void onErrorResponse(VolleyError error, Object requestTag) {

            }

            @Override
            public void onErrorResponse(VolleyError error, NetworkResponse response, Object requestTAG, Object requestParams) {

            }
        }), new HashMap<>());
    }


}
