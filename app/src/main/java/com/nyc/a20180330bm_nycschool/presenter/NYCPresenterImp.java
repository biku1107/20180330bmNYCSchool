package com.nyc.a20180330bm_nycschool.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.nyc.a20180330bm_nycschool.application.NYCApplication;
import com.nyc.a20180330bm_nycschool.model.NYCSchool;
import com.nyc.a20180330bm_nycschool.presenter.api.IHttpConnection;
import com.nyc.a20180330bm_nycschool.presenter.api.IResponseCallback;
import com.nyc.a20180330bm_nycschool.view.INYCView;

import java.util.HashMap;

import static com.nyc.a20180330bm_nycschool.application.AppConstant.*;


/**
 * Created by Bikash on 3/30/2018.
 * Presenter implementation class for NYC school
 */

public class NYCPresenterImp implements INYCPresenter,IResponseCallback {

    private static final String TAG = NYCPresenterImp.class.getSimpleName();
    private static NYCPresenterImp mSelf = null;
    private INYCView mView ;

    public NYCPresenterImp(INYCView aView) {
        this.mView = aView;
    }

    /**
     * Forming NYC school result  request
     * @param responseCallback response received from the server
     */
    @Override
    public synchronized void getSchoolScore(IResponseCallback responseCallback) {
        Log.d(TAG,"getWeatherContent()");
        NetworkController gatewayController = NetworkController.getInstance(NYCApplication.getAppContext() , responseCallback);
        HashMap<String, String> params = new HashMap<>();
        gatewayController.processNetworkRequest(IHttpConnection.IResponseObserver.RequestTypeEnum.GET_SCHOOL_RESULT,params, Request.Priority.IMMEDIATE);
    }

    /**
     * Response received from server
     * @param status server response status to validate success or error
     * @param body Response data recived from server
     * @param aRespType Response received for which request type
     * @param requestParams Request param to distinguish response received for which request.
     */
    @Override
    public synchronized  void responseReceived(int status, String body, IHttpConnection.IResponseObserver.RequestTypeEnum aRespType, Object requestParams) {

        Log.d(TAG, "response received resType is " + aRespType + " status " + status + " body message received =" + body);
        Log.d(TAG, "response received resType is " + aRespType + " status " + status + " body message received =" + body);
        switch (aRespType) {
            case POST_CONTENT:
                break;
            case GET_SCHOOL_RESULT:
                switch (status) {
                    case SUCCESS_OK:
                    case NOT_MODIFIED:
                        parseResponse(body,aRespType, requestParams);
                        break;
                }
                break;
        }
    }

    /**
     * Parser to parse data received from server
     * @param aResponse response data
     * @param aRespType Request type to distiguish
     * @param aRequestParam request param sent
     */
    public  void parseResponse(String aResponse, IHttpConnection.IResponseObserver.RequestTypeEnum aRespType, Object aRequestParam) {

        Log.d(TAG , "parseResponse() - response " +aResponse);
        Gson lGson = new Gson();
        try {
            NYCSchool[] schoolResult = lGson.fromJson(aResponse, NYCSchool[].class);
            Log.d(TAG , "DBN " + schoolResult[0].getDbn()  + " School Name " +schoolResult[0].getSchool_name());
            if( mView!= null) {
                mView.notifyDataChange(schoolResult);
            }
        }catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }

    }

}
