package com.nyc.a20180330bm_nycschool.presenter;

import android.location.Location;

import com.nyc.a20180330bm_nycschool.presenter.api.IResponseCallback;


/**
 * Created by Bikash on 3/30/2018.
 */

public interface INYCPresenter {
    /**
     * Receive School score from NYC server
     * @param responseCallback Response call back to receive the response for the request
     */
    void getSchoolScore(IResponseCallback responseCallback) ;

}
