package com.nyc.a20180330bm_nycschool.presenter;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nyc.a20180330bm_nycschool.application.AppConstant;
import com.nyc.a20180330bm_nycschool.presenter.api.IHttpConnection;
import com.nyc.a20180330bm_nycschool.presenter.api.IResponseCallback;
import com.nyc.networksdk.JobQueue.PriorityJobQueue;
import com.nyc.networksdk.Listener.ErrorResponseListener;
import com.nyc.networksdk.Listener.ResponseListener;
import com.nyc.networksdk.Network.Constants;
import com.nyc.networksdk.Network.GetRequest;
import com.nyc.networksdk.Network.NetworkHandler;
import com.nyc.networksdk.Network.PostRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nyc.a20180330bm_nycschool.application.AppConstant.*;


/**
 * Created by Bikash on 3/30/2018.
 * All request and response wrapper to process the request and get the response from server.
 * It uses custom network wrapper module which was developed on top of Volley as an wrapper with all the required Rest API services.
 */

public class NetworkController implements ResponseListener.Listener,ErrorResponseListener.ErrorListener{
    private static NetworkController instance = null;
    private IResponseCallback presenter;
    private Context mContext;
    /**
     * Network handler instance for network calls defined on Network sdk module.
     *to process all N/w requests and get the response
     */
    private NetworkHandler networkHandler;
    /**
     * Define the request priority to process from the queue, if IMMEDIATE then it has to process with higher priority than others
     */
    private PriorityJobQueue priorityJobQueue;
    /**
     * Request type to distinguish between the request.
     */
    private IHttpConnection.IResponseObserver.RequestTypeEnum mResponseType;
    /**
     * Tag for logging
     */
    private String TAG = NetworkController.class.getName();

    /**
     * Constructor to initialize objects of NetwrorkController
     * @param aCxt Context object
     * @param presenter presenter instance to send the response received from the server.
     */
    private NetworkController(Context aCxt , IResponseCallback presenter) {
        this.mContext = aCxt;
        this.presenter = presenter;
        this.networkHandler = NetworkHandler.getInstance(mContext);
        this.priorityJobQueue = networkHandler.getJobQueue();
        Log.d(TAG, "Inside GatewayController ()");
    }
    public static NetworkController getInstance(Context mContext , IResponseCallback presenter) {
        if (instance == null) instance = new NetworkController(mContext , presenter);
        return instance;
    }

    /**
     * Methods to process the network request
     */


    public void processNetworkRequest(IHttpConnection.IResponseObserver.RequestTypeEnum mResponseType, Object requestParams, Request.Priority priority) {
        this.mResponseType = mResponseType;
        switch (mResponseType) {
            case POST_CONTENT:
                Log.v(TAG, "LOGIN_AUTH_SERVICE processNetworkRequest()-->");
                postRequest(requestParams, mResponseType, priority);
                break;
            case GET_SCHOOL_RESULT:
                Log.v(TAG, "Passtime processNetworkRequest()-->");
                getSatResult(requestParams, mResponseType, priority);
                break;

        }
    }

    /**
     * make the network request for NYC school
     * @param requestParams Request param to create the request
     * @param mResponseType request type to distinguish the request for response received.
     * @param priority Priority to process the request, if Immediate then it has to be process with highest priority
     *                 and other request will be still on the queue even if it requested earlier.
     */
    private void getSatResult(Object requestParams, IHttpConnection.IResponseObserver.RequestTypeEnum mResponseType, Request.Priority priority) {
        HashMap<String, String> params = (HashMap<String, String>) requestParams;
        Log.v(TAG, "getWeatherContent() Request-->");

        Log.d(TAG, "Request is about to get the Contents  = ");
        //Hard coded URL , It can be put on config file
        String lUrl = AppConstant.SAT_URI;

        //creating get request to process
        GetRequest getSanboxdata = (GetRequest) networkHandler.getRequestObject(Constants.NetworkRequestType.GET, mResponseType, lUrl, new ResponseListener(this), new ErrorResponseListener(this), requestParams);
        Log.d(TAG, "List Data Request() ->  Url=" + lUrl);
        // added priority
        getSanboxdata.setPriority(priority);
        //set retry mechanism
        getSanboxdata.setRetryPolicy(new DefaultRetryPolicy(SOCKET_TIMEOUT_TIME, RE_TRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //added to Volley request queue for processing
        priorityJobQueue.addToRequestQueue(getSanboxdata, TAG_CONTENT_RESPONSE_TYPE);
    }

    /**
     * make the network request for NYC school
     * @param requestParams Request param to create the request
     * @param mResponseType request type to distinguish the request for response received.
     * @param priority Priority to process the request, if Immediate then it has to be process with highest priority
     *                 and other request will be still on the queue even if it requested earlier.
     */
    private void getSchools(Object requestParams, IHttpConnection.IResponseObserver.RequestTypeEnum mResponseType, Request.Priority priority) {
        HashMap<String, String> params = (HashMap<String, String>) requestParams;
        Log.v(TAG, "getWeatherContent() Request-->");

        Log.d(TAG, "Request is about to get the Contents  = ");
        //Hard coded URL , It can be put on config file
        String lUrl = AppConstant.HIGHSCHOOL_URI;


        GetRequest getSanboxdata = (GetRequest) networkHandler.getRequestObject(Constants.NetworkRequestType.GET, mResponseType, lUrl, new ResponseListener(this), new ErrorResponseListener(this), requestParams);
        Log.d(TAG, "List Data Request() ->  Url=" + lUrl);
        getSanboxdata.setPriority(priority);
        getSanboxdata.setRetryPolicy(new DefaultRetryPolicy(SOCKET_TIMEOUT_TIME, RE_TRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        priorityJobQueue.addToRequestQueue(getSanboxdata, TAG_CONTENT_RESPONSE_TYPE);
    }


    /**
     * make the post network request for NYC school, Kept just for demonstration not in use.
     * @param requestParams Request param to create the request
     * @param mResponseType request type to distinguish the request for response received.
     * @param priority Priority to process the request, if Immediate then it has to be process with highest priority
     *                 and other request will be still on the queue even if it requested earlier.
     */
    private void postRequest(Object requestParams, IHttpConnection.IResponseObserver.RequestTypeEnum mResponseType, Request.Priority priority) {
        Log.v(TAG, "postRequestpostRequest()-->");
        if (requestParams instanceof HashMap) {
            Log.v(TAG, "postRequest postRequest()-->requestParams check");
            HashMap<String, String> params = (HashMap<String, String>) requestParams;
            //TODO write the logic to make network call
            JSONObject luserRegistrationdata = new JSONObject();

            String lUrl = "http://lyrics.wikia.com/api.php?func=getSong&artist=Tom+Waits&song=new+coat+of+paint&fmt=json";
            // String lUrl = "https://user-auth-service-uat.cfapps.scus-10.test.cf.fedex.com/v1/user/auth";
            int requestType = Constants.NetworkRequestType.POST;
            PostRequest authRequest = (PostRequest) networkHandler.getRequestObject(requestType, mResponseType, lUrl, new ResponseListener(this), new ErrorResponseListener(this), requestParams);
            authRequest.addHeader("Content-Type", "application/json");
            authRequest.addHeader("X-locale", "en_US"); // As per working param
            authRequest.addHeader("X-version", "1");
            authRequest.setRequestBody(luserRegistrationdata.toString());
            authRequest.setEncodingType("utf-8");
            authRequest.setPriority(priority);
            authRequest.setRetryPolicy(new DefaultRetryPolicy(SOCKET_TIMEOUT_TIME, RE_TRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            priorityJobQueue.addToRequestQueue(authRequest, TAG_POST_RESPONSE_TYPE);
        }
    }


    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onResponseHeaders(Map<String, String> headers, Object requestTAG) {

    }

    /**
     * Response rec eived from Server
     * @param response Network response object received from server, like status code etc
     * @param responseObject  response body receive from response object result.
     * @param requestTAG response received for which request tag
     * @param requestParams request param
     */
    @Override
    public void onResponseObject(NetworkResponse response, Response<String> responseObject, Object requestTAG, Object requestParams) {

        Log.v(TAG, "onResponseObject()-->pre");
        IHttpConnection.IResponseObserver.RequestTypeEnum mResponseTypeFromRequest = (IHttpConnection.IResponseObserver.RequestTypeEnum) requestTAG;
        //Reset if response received
        presenter.responseReceived(response.statusCode, responseObject.result, mResponseTypeFromRequest, requestParams);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onErrorResponse(VolleyError error, Object requestTag) {

    }

    /**
     * Error response received
     * @param error The Volley error input
     * @param response Response Object from Volley
     * @param requestTAG The request tag
     * @param requestParams The request parameters that are passed to request.
     */
    @Override
    public void onErrorResponse(VolleyError error, NetworkResponse response, Object requestTAG, Object requestParams) {
        IHttpConnection.IResponseObserver.RequestTypeEnum mResponseTypeFromRequest = (IHttpConnection.IResponseObserver.RequestTypeEnum) requestTAG;
        try {

            if (response == null || (response != null && (Integer) response.statusCode == null)) {
                presenter.responseReceived(-1, null, mResponseTypeFromRequest, requestParams);
            } else {
                presenter.responseReceived(response.statusCode, null, mResponseTypeFromRequest, requestParams);
            }
        } catch (Exception ex) {
            presenter.responseReceived(-1, null, mResponseTypeFromRequest, requestParams);
            ex.printStackTrace();
            Log.e(TAG, "error==" + ex.toString());
        }
    }
}
