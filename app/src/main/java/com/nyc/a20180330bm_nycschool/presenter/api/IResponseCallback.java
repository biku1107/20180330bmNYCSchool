package com.nyc.a20180330bm_nycschool.presenter.api;

/**
 * Created by Bikash on 3/30/2018.
 * Response callback interface to receive the response from server
 */

public interface IResponseCallback {
    /**
     * Response received from server
     * @param status server response status to validate success or error
     * @param body Response data recived from server
     * @param aRespType Response received for which request type
     * @param requestParams Request param to distinguish response received for which request.
     */
    void responseReceived(int status, String body, IHttpConnection.IResponseObserver.RequestTypeEnum aRespType, Object requestParams) ;
}
