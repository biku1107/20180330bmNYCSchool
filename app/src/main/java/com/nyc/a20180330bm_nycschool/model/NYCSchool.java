package com.nyc.a20180330bm_nycschool.model;

/**
 * Created by Bikash on 3/30/2018.
 *

 }

 */

public class NYCSchool {

    /**
     * NYC schoold DBN code
     */
    String dbn;
    /**
     * NYC school number of Sat test takers
     */
    String num_of_sat_test_takers;
    /**
     * NYC school Sat critical reading average scores
     */
    String sat_critical_reading_avg_score;
    /**
     * NYC school Math average scores
     */
    String sat_math_avg_score;
    /**
     * Number of Sat written average score
     */
    String sat_writing_avg_score;
    /**
     * NYC School name
     */
    String school_name;

    /**
     * Return School DBN
     * @return String DBN code
     */
    public String getDbn() {
        return dbn;
    }

    /**
     * Set School DBN
     * @param dbn code
     */
    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    /**
     * Return number test takers.
     * @return String Number of test takers.
     */
    public String getNum_of_sat_test_takers() {
        return num_of_sat_test_takers;
    }
    
    public void setNum_of_sat_test_takers(String num_of_sat_test_takers) {
        this.num_of_sat_test_takers = num_of_sat_test_takers;
    }

    public String getSat_critical_reading_avg_score() {
        return sat_critical_reading_avg_score;
    }

    public void setSat_critical_reading_avg_score(String sat_critical_reading_avg_score) {
        this.sat_critical_reading_avg_score = sat_critical_reading_avg_score;
    }

    public String getSat_math_avg_score() {
        return sat_math_avg_score;
    }

    public void setSat_math_avg_score(String sat_math_avg_score) {
        this.sat_math_avg_score = sat_math_avg_score;
    }

    public String getSat_writing_avg_score() {
        return sat_writing_avg_score;
    }

    public void setSat_writing_avg_score(String sat_writing_avg_score) {
        this.sat_writing_avg_score = sat_writing_avg_score;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

}
