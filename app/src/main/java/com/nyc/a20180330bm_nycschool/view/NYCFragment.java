package com.nyc.a20180330bm_nycschool.view;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nyc.a20180330bm_nycschool.R;
import com.nyc.a20180330bm_nycschool.application.AppConstant;
import com.nyc.a20180330bm_nycschool.model.NYCSchool;
import com.nyc.a20180330bm_nycschool.presenter.NYCPresenterImp;
import com.nyc.a20180330bm_nycschool.view.adapter.NYCAdapter;
import com.nyc.a20180330bm_nycschool.view.widget.NYCLinearLayoutManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Bikash on 3/30/2018.
 */

public class NYCFragment extends Fragment implements INYCView {


    private NYCAdapter mNycAdapter;
    private ArrayList<NYCSchool> mRecyclerItemList = new ArrayList<NYCSchool>();
    NYCPresenterImp preseterimpl;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retain this fragment across configuration changes.
        setRetainInstance(true);

        preseterimpl = new NYCPresenterImp(this);


    }

    @Override
    public void onResume() {
        super.onResume();
        preseterimpl.getSchoolScore(preseterimpl);
    }
    @BindView(R.id.txtview_welcome_name)
    TextView welcomename ;
    @BindView(R.id.recycler_view)
    public RecyclerView mRecyclerViewScroll;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View lView = inflater.inflate(R.layout.school_view, container, false);
        ButterKnife.bind(this,lView);
        welcomename.setText(getText(R.string.welcome));
        prepareRecyclerView();
        return lView;
    }

    /**
     * Preparing to load the view
     */
    private void prepareRecyclerView() {
        mNycAdapter = new NYCAdapter(getActivity(), mRecyclerItemList);

        NYCLinearLayoutManager wmLayout = new NYCLinearLayoutManager(getContext());
        mRecyclerViewScroll.setLayoutManager(wmLayout);

        mRecyclerViewScroll.setAdapter(mNycAdapter);
        mRecyclerViewScroll.setNestedScrollingEnabled(true);
        mRecyclerViewScroll.setHasFixedSize(true);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void notifyDataChange(final NYCSchool[] nycSchools) {

        /**
         * Handler to post the data to UI thread
         */
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                mRecyclerItemList.clear();

                for (NYCSchool aSchoolRes : nycSchools) {
                    mRecyclerItemList.add(aSchoolRes);
                    AppConstant.mSchoolsData.put(aSchoolRes.getSchool_name() , aSchoolRes);
                }

                if (mRecyclerItemList.size() > 0) {
                    mNycAdapter.notifyDataSetChanged();
                }
            }
        });

    }

}
