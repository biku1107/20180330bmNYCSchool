package com.nyc.a20180330bm_nycschool.view.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nyc.a20180330bm_nycschool.R;
import com.nyc.a20180330bm_nycschool.application.AppConstant;
import com.nyc.a20180330bm_nycschool.model.NYCSchool;
import com.nyc.a20180330bm_nycschool.view.SchoolDetailActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Bikash
 * Adapter class to list down the NYC school results

 */

public class NYCAdapter extends RecyclerView.Adapter<NYCAdapter.NYCViewHolder> {

    private static final String TAG = NYCAdapter.class.getSimpleName();
    /**
     * Current Context
     */
    private Context mContext;
    private ArrayList<NYCSchool> mAdapterItemList = new ArrayList<NYCSchool>();

    private View mView;

    public NYCAdapter(Activity mContext, ArrayList<NYCSchool> aAdapterItemList) {
        super();
        this.mContext = mContext;
        this.mAdapterItemList = aAdapterItemList;
    }

    @Override
    public NYCViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(mContext).inflate(R.layout.school_rcylr_item, parent, false);
        return new NYCViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(NYCViewHolder holder, final int position) {
        Log.d(TAG , "onBindViewHolder" );
        if (holder!=null  && holder instanceof NYCViewHolder) {
            //Data Received from the server

            holder.schoolname.setText(mAdapterItemList.get(position).getSchool_name());
            holder.dbn.setText(mAdapterItemList.get(position).getSat_math_avg_score());

            holder.mItemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    CharSequence text = ((TextView)mView.findViewById(R.id.schoolname)).getText();
                    Log.d(TAG , "text "+text.toString());
                    //Hack Layout view text is not giving correct text.
                    String key = mAdapterItemList.get(position ).getSchool_name();

                    AppConstant.mSelectedSchool = text.toString();
                    //Launchng activity on Layout click for selected school result details.
                    Intent launchDeatils = new Intent(view.getContext() , SchoolDetailActivity.class);
                    launchDeatils.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    launchDeatils.putExtra(AppConstant.SCHOOL_KEY, key);
                    view.getContext().startActivity(launchDeatils);
                }
            });
        }

    }
    @Override
    public void onViewAttachedToWindow(NYCViewHolder holder) {
        super.onViewAttachedToWindow(holder);

    }
    @Override
    public int getItemCount() {

        return mAdapterItemList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Update adapter list
     * @param aRecyclerItemList updated result list
     */
    public void updateAdapterList(ArrayList<NYCSchool> aRecyclerItemList) {
//        mAdapterItemList.clear();
        mAdapterItemList = aRecyclerItemList;
        notifyDataSetChanged();


    }

    /**
     * View holder for adaptor.
     */
    public class NYCViewHolder extends RecyclerView.ViewHolder {

        /**
         * Binding view holder by the help of butter knife.
         */

        @BindView(R.id.schoolname)
        TextView schoolname;
        @BindView(R.id.dbn)
        TextView dbn;
        @BindView(R.id.item_layout)
        RelativeLayout mItemLayout;

        public NYCViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }
}
