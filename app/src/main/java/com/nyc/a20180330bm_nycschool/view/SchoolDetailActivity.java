package com.nyc.a20180330bm_nycschool.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.nyc.a20180330bm_nycschool.R;
import com.nyc.a20180330bm_nycschool.application.AppConstant;
import com.nyc.a20180330bm_nycschool.model.NYCSchool;


public class SchoolDetailActivity extends AppCompatActivity {

    private static final String TAG = SchoolDetailActivity.class.getCanonicalName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.school_details);

        String schoolName = getIntent().getExtras().getString(AppConstant.SCHOOL_KEY);
        TextView schoolname = findViewById(R.id.schoolname);
        TextView mathAvgRatg = findViewById(R.id.math_avg_score);
        TextView criticalavgScore = findViewById(R.id.critical_avg_score);
        TextView dbn = findViewById(R.id.dbn);
        TextView writingavgScore = findViewById(R.id.writing_avg_score);

        if(schoolName != null && !schoolName.isEmpty()) {
            NYCSchool schoolDetails = AppConstant.mSchoolsData.get(schoolName);
            schoolname.setText(schoolDetails.getSchool_name());
            mathAvgRatg.setText(schoolDetails.getSat_math_avg_score());
            criticalavgScore.setText(schoolDetails.getSat_critical_reading_avg_score());
            dbn.setText(schoolDetails.getDbn());
            writingavgScore.setText(schoolDetails.getSat_writing_avg_score());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(android.R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
}
