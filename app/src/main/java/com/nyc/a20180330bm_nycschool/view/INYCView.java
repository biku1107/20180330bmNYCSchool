package com.nyc.a20180330bm_nycschool.view;

import com.nyc.a20180330bm_nycschool.model.NYCSchool;


/**
 * Created by Bikash on 3/30/2018.
 * View interface to exposed to presenter layer
 */

public interface INYCView {
    /**
     * Notify to the UI for data changed on presenter layer
     * @param schoolResult Scholl Result data received from the server.
     */
    public void notifyDataChange(NYCSchool[] schoolResult);
}
