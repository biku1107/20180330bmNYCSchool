package com.nyc.a20180330bm_nycschool.view.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;


/**
 * Created by Bikash.K on 3/30/2016.
 * Custom Linear layout to handle the scrolling .
 */
public class NYCLinearLayoutManager extends LinearLayoutManager {
    private boolean isScrollEnabled = true;
    private static final String TAG = NYCLinearLayoutManager.class.getName();



    public NYCLinearLayoutManager(Context context) {
        super(context);
    }

    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;
    }


    @Override
    public boolean canScrollVertically() {
        //Similarly you can customize "canScrollHorizontally()" for managing horizontal scroll
        //return isScrollEnabled && super.canScrollVertically();
        return isScrollEnabled;
    }


}
