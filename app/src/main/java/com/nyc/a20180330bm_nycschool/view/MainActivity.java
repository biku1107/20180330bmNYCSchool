package com.nyc.a20180330bm_nycschool.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.nyc.a20180330bm_nycschool.R;
import com.nyc.a20180330bm_nycschool.view.adapter.NYCAdapter;


public class MainActivity extends AppCompatActivity  {

    private static final String TAG = MainActivity.class.getCanonicalName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFragment();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**

     * Return the current state of the permissions needed.

     */
    private boolean checkPermissions() {

        int permissionState = ActivityCompat.checkSelfPermission(this,

                Manifest.permission.ACCESS_FINE_LOCATION);

        return permissionState == PackageManager.PERMISSION_GRANTED;

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(android.R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    private void loadFragment()
    {
        String tag = "NYCFragment";
        FragmentManager mFragmentManager = getSupportFragmentManager();
        NYCFragment issFragment = (NYCFragment) mFragmentManager.findFragmentByTag(tag);

        // If the Fragment is non-null, then it is currently being
        // retained across a configuration change.
        if (issFragment == null) {
            issFragment = new NYCFragment();
             mFragmentManager.beginTransaction().add(R.id.container_iss_activity, issFragment, tag).commit();
        }

        Log.d(TAG, "loadFragment() -> All Layout loaded ");
    }

}
