package com.nyc.a20180330bm_nycschool.application;

import com.nyc.a20180330bm_nycschool.model.NYCSchool;

import java.util.HashMap;

/**
 * Created by Bikash on 03/30/2018.
 */

public class AppConstant {
    //    public static final String HIGHSCHOOL_URI = "https://data.cityofnewyork.us/resource/734v-jeq5.json";
    public static final String HIGHSCHOOL_URI = "http://data.cityofnewyork.us/resource/97mf-9njv.json";
    public static final String SAT_URI = "http://data.cityofnewyork.us/resource/734v-jeq5.json";
    public static final String SCHOOL_KEY ="schoolname";
    public static String mSelectedSchool ;
    public static HashMap<String,NYCSchool> mSchoolsData = new HashMap<>();

    public static int SOCKET_TIMEOUT_TIME = 3000;
    public static int RE_TRY_COUNT = 3;
    public static final String TAG_CONTENT_RESPONSE_TYPE = "get-content";
    public static final String TAG_POST_RESPONSE_TYPE = "post-content";

    public static final int SUCCESS_OK = 200;
    public static final int CREATED = 201;
    public static final int ACCEPTED = 202;
    public static final int FAILURE_CONNECTION = -1;
    public static final int BAD_REQUEST = 400;
    public static final int UNAUTHORIZED = 401;
    public static final int CREDENTIAL_CHANGE = 402;
    public static final int FORBIDDEN = 403;
    public static final int FILE_NOT_FOUND = 404;
    public static final int INTERNAL_SERVER_ERROR = 500;
    public static final int NOT_MODIFIED = 304;
}
