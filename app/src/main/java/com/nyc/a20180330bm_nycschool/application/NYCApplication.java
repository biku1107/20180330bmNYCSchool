package com.nyc.a20180330bm_nycschool.application;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.util.List;


/**
 * Created by Bikash on 3/30/2018.
 *
 */

public class NYCApplication extends Application {

    private static final String TAG = NYCApplication.class.getSimpleName();
    private static Context mApplicationcontext;
    private Typeface universeRegular;
    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationcontext = getApplicationContext();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }

    /**
     * Return Application context
     * @return Context
     *          App context
     */
    public static Context getAppContext() {
        return mApplicationcontext;
    }

    /**
     * Set the application context
     * @param aCxt application context
     */
    public static void setAppContext(Context aCxt) {
        mApplicationcontext = aCxt;
    }

    /**
     * Kill the application
     */
    public static void killApplication() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    /**
     * Check for network is up or not
     * @return
     */
    public static boolean isNetworkUp() {
        try{
            NetworkInfo info = ((ConnectivityManager) getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (info == null || !info.isConnected()) {
                Log.d(TAG,"NetworkCheck Not connected");
                return false;
            }
        }catch (Exception e) {
            Log.w(TAG,"Exception while checking Network Up ", e);
        }
        Log.v(TAG,"NetworkCheck connected");
        return true;
    }

    /**
     * Check for app is running or not
     * @param aCxt Application context
     * @return boolean true if app is running else false
     */
    public static boolean isAppRunning(Context aCxt) {
        ActivityManager activityManager = (ActivityManager) aCxt.getSystemService( ACTIVITY_SERVICE );
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for(int i = 0; i < procInfos.size(); i++)
        {
            if(procInfos.get(i).processName.equals("iss.chase.com.ispacestation"))
            {
                return true;
            }
        }
        return false;
    }

}
