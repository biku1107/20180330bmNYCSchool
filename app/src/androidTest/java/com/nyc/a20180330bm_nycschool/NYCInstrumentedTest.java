package com.nyc.a20180330bm_nycschool;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.nyc.a20180330bm_nycschool.presenter.INYCPresenter;
import com.nyc.a20180330bm_nycschool.presenter.api.IHttpConnection;
import com.nyc.a20180330bm_nycschool.presenter.api.IResponseCallback;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class NYCInstrumentedTest {
    @Mock
    INYCPresenter iNycPresenter;
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();



    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.nyc.a20180330bm_nycschool", appContext.getPackageName());
    }
    @Test
    public void getSchoolScoreTest() {
        iNycPresenter.getSchoolScore( new IResponseCallback() {
            @Override
            public void responseReceived(int status, String body, IHttpConnection.IResponseObserver.RequestTypeEnum aRespType, Object requestParams) {
                assertEquals(200, status);
                assertNotNull(body);
                assertEquals(IHttpConnection.IResponseObserver.RequestTypeEnum.GET_SCHOOL_RESULT , aRespType);


            }
        });
    }

}
